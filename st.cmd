#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(nmx-chpsy1-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")
iocshLoad("./nmx-chpsy1-sc-ioc-001.iocsh","IPADDR=172.30.44.00,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"BIFROST-ChpSy1:")
epicsEnvSet(R1,"Chop-WLS-101:")
epicsEnvSet(R2,"Chop-WLS-201:")
epicsEnvSet(R3,"Chop-WLS-202:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R1)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R2)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R3)")

iocInit()
#EOF
